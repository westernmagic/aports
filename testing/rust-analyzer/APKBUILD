# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2022.03.14
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-analyzer/rust-analyzer"
# limited by rust, x86/armhf/armv7 fail tests
arch="x86_64 aarch64 ppc64le"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-analyzer/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin target/release/rust-analyzer

	install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE-MIT LICENSE-APACHE
	install -Dm644 docs/user/manual.adoc "$pkgdir"/usr/share/doc/$pkgname/manual.adoc
}

sha512sums="
0ef9afda5ae9aea58901596f6b85a4cf4998ca56b0655066b24c7e9a90a956de336d2f69096f344c28cfbd27bb7186f1b52cbce99e060d1cf4951d95521d2b4a  rust-analyzer-2022.03.14.tar.gz
"
